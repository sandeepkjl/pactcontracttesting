package com.example.payment.paymentapi.controller;

import com.example.payment.paymentapi.model.PaymentRequest;
import com.example.payment.paymentapi.model.PaymentResponse;
import com.example.payment.paymentapi.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService service;

    @Value("${limit.api.url}")
    private String limitUrl;


    @PostMapping("/doPayment")
    public ResponseEntity<PaymentResponse> doPayment(@RequestBody PaymentRequest request) {

        PaymentResponse response = service.intiatePayment(request, limitUrl);

        return new ResponseEntity<PaymentResponse>(response, HttpStatus.OK);
    }

    public void setLimitUrl(String limitUrl){
        this.limitUrl=limitUrl;
    }
}
