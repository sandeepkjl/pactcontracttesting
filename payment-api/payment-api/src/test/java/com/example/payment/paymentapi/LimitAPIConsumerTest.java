package com.example.payment.paymentapi;


import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import com.example.payment.paymentapi.controller.PaymentController;
import com.example.payment.paymentapi.model.PaymentRequest;
import com.example.payment.paymentapi.model.PaymentResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "LimitProvider")
public class LimitAPIConsumerTest {

    @Autowired
    PaymentController paymentController;

    @Pact(provider="LimitProvider", consumer="PaymentApi")
    public RequestResponsePact createPact(PactDslWithProvider builder) {
        return builder
                .given("test state")
                .uponReceiving("limit check request")
                .path("/checkLimit/12345")
                .method("GET")
                .willRespondWith()
                .status(200)
                .body(new PactDslJsonBody()
                .integerType("accountId")
                .integerType("dailyLimit")
                .integerType("remainingLimit"))
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "createPact", port = "9999")
    //MockServer is a class in pact library, which have the information where pact server is running.
    public void testDoPayment(MockServer mockServer) {
        paymentController.setLimitUrl(mockServer.getUrl());

        PaymentRequest request = new PaymentRequest();
        request.setFromAccountId(12345);
        request.setPaymentAmount(10000);
        request.setToAccountId(54321);

        ResponseEntity<PaymentResponse> memberListResponse = paymentController.doPayment(request);
        System.out.println(memberListResponse.getBody());
        Assertions.assertEquals(HttpStatus.OK, memberListResponse.getStatusCode());

    }


}
